// eslint-disable-next-line no-unused-vars
const mongodb = require('./src/connections/mongodb.connection');
const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger');

const express = require('express');
const app = express();
const port = process.env.PORT || 7007;

const basketRoutes = require('./src/routes/basket.routes');
const bodyParser = require('body-parser');

const { eurekaClient } = require('./src/environment/config/eureka.config');

// const rabbitmq = require('./src/connections/rabbitmq.connection');
// const messageSender = require('./src/eventhandlers/message.sender');
// const messageReceiver = require('./src/eventhandlers/message.receiver');

app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/baskets', basketRoutes);

// default:
app.use('*', (req, res) => {
  res.status(400).send({
    error: 'not available',
  });
});

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
