const sendSomethingWentWrongResponse = res => {
  res.status(400).send('Something went wrong.');
};

module.exports = {
  sendSomethingWentWrongResponse,
};
