const ip = require('ip');
const Eureka = require('eureka-js-client').Eureka;

const ipAddr = process.env.IP_ADDRESS || ip.address();
const hostName = process.env.HOST_NAME || ip.address();
const port = process.env.PORT || 7007;

const eurekaClient = new Eureka({
  // application instance information
  instance: {
    app: 'basket-management',
    hostName: hostName,
    ipAddr: ipAddr,
    homePagekUrl: `http://${ipAddr}:${port}`,
    port: {
      $: port,
      '@enabled': 'true',
    },
    vipAddress: 'basket-management',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    leaseInfo: {
      renewalIntervalInSecs: 10,
      durationInSecs: 30
    }
  },
  eureka: {
    preferIpAddress: true,
    maxRetries: 10,
    registerWithEureka: true,
    fetchRegistry: true,
    serviceUrls: {
      default: [
        'http://eurekaserver-1:8761/eureka/apps/', 
        'http://eurekaserver-2:8762/eureka/apps/'
      ]
    }
  },
});

module.exports = { eurekaClient };
