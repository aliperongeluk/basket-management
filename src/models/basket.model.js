const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Basket:
 *    type: object
 *    properties:
 *      productIds:
 *        type: string
 *      customerInformationId:
 *        type: string
 */


const BasketSchema = new Schema(
  {
    productIds: {
      type: [String],
      required: [true, 'at least one product is required.'],
    },
    customerInformationId: {
      type: String,
      required: [false],
    }
  },
  { timestamps: true }
);

const Basket = mongoose.model('Basket', BasketSchema);

module.exports = Basket;
