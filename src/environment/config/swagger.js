const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Basket Management',
      version: '1.0.0',
      description:
        'Swagger documentation for Basket Management microservice (AliPerOngeluk).',
    },
  },

  apis: ['./src/routes/*.js', './src/models/*.js'],
};

const specs = swaggerJsDoc(options);

module.exports = specs;
