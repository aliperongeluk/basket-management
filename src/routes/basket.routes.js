const express = require('express');
const routes = express.Router();
const Basket = require('../models/basket.model');
const { sendSomethingWentWrongResponse } = require('../utils/response');

/**
 * @swagger
 * /api/basket-management/baskets:
 *    get:
 *     tags:
 *       - baskets
 *     description: Retrieving all baskets.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Baskets are returned.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Basket'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', async (req, res) => {
  try {
    const baskets = await Basket.find();
    res.send(baskets);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/basket-management/baskets/{id}:
 *    get:
 *     tags:
 *       - baskets
 *     description: Retrieving basket with given ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *         type: string
 *         required: true
 *         description: String ID of a customer.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Basket with given Id is returned.
 *          schema:
 *              $ref: '#/definitions/Basket'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    const basket = await Basket.findById(id);
    res.send(basket);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/basket-management/baskets/products:
 *    post:
 *     tags:
 *       - baskets
 *     description: Creating new basket in basket database. Also adds the newly created basket to the message broker.
 *     parameters:
 *        - in: body
 *          name: body
 *          description: Create new basket object.
 *          schema:
 *            type: object
 *            required: true
 *            properties:
 *              productIds:
 *                type: [string]
 *              customerId:
 *                type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Basket is saved in database and returned.
 *          schema:
 *            type: object
 *            items:
 *              $ref: '#/definitions/Basket'
 *       400:
 *          description: Something went wrong.
 */
routes.post('/products', (req, res) => {
  res.send('TODO: POST');
});

/**
 * @swagger
 * /api/basket-management/baskets/products/{id}:
 *    delete:
 *     tags:
 *       - baskets
 *     description: Delete basket with given ID from database.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *         type: string
 *         required: true
 *         description: String ID of the basket.
 *     responses:
 *       200:
 *          description: Basket is deleted and returned.
 *          schema:
 *            $ref: '#/definitions/Basket'
 *       400:
 *          description: Something went wrong.
 */
routes.delete('/products/:id', (req, res) => {
  res.send('TODO: DELETE');
});

module.exports = routes;
